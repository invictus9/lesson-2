import React from "react";
import "./TodoNote.css";

class TodoNote extends React.Component {
    constructor(props) {
        super(props);

        // this.deleteHandleNote = () => {
        //     this.props.deleteNote(this.props.id)
        // }

        this.completeNote = () => {
            this.props.completeNote(this.props.id);
        }
    }

    render () {
        return (
            <div className={this.props.note.isDone ? "note noteCompleted": "note"} onClick={this.completeNote}>
                <img className={this.props.note.isDone ? "imgNoteCompleted" : "imgNoteUnCompleted"} src="images/success.png"></img>    
                {this.props.note.text}
                {
                    //<img src="images/delete.png"></img>
                }

            </div>
        )
    }
}

export default TodoNote;