import React from "react";
 import TodoList from "./TodoList.jsx";
 import TodoInput from "./TodoInput.jsx";
 import TodoManager from "./TodoManager.jsx"
 import "./TodoApp.css"

class TodoApp extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            notes: [],
            filterNotes: [],
            activeNotes: 0
        }

        this.handleAddNote = (newNote) => {
            const notes = this.state.notes;
            notes.unshift(newNote);
            this.setState({
                notes: notes,
                filterNotes: notes,
                activeNotes: this.state.activeNotes + 1
            })
        }

        // this.handleDeleteNote = (deleteNoteId) => {
        //     const notes = this.state.notes;
        //     let newNotes = notes.filter((note) => {
        //         return note.id !== deleteNoteId;
        //     });
        //     this.setState({
        //         notes: newNotes,
        //         filterNotes: newNotes
        //     })
        // }

        this.completeNote = (note_id) => {
            let notes = this.state.notes;
            for (let i = 0; i < notes.length; ++i) {
                if (notes[i].id === note_id) {
                    if (notes[i].isDone == true) {
                            notes[i].isDone = false;
                            this.setState({
                                notes: notes,
                                filterNote: notes,
                                activeNotes: this.state.activeNotes + 1 
                            })
                    } else {
                        notes[i].isDone = true;
                        this.setState({
                            notes: notes,
                            filterNote: notes,
                            activeNotes: this.state.activeNotes - 1 
                        })
                    }

                    break;
                }
            }
            
        }

        this.viewAllNotes = (event) => {
            event.preventDefault();
            this.setState({
                filterNotes: this.state.notes
            })
        }

        this.viewCompletedNotes = (event) => {
            event.preventDefault();
            let notes = this.state.notes.filter( (note) => {
                return note.isDone === true;
            })
            this.setState({
                filterNotes: notes
            })
        }

        this.viewActiveNotes = (event) => {
            event.preventDefault();
            let notes = this.state.notes.filter( (note) => {
                return note.isDone === false;
            })
            this.setState({
                filterNotes: notes
            })
        }

        this.deleteCompleted = (event) => {
            event.preventDefault();
            if (this.state.notes.length > 0) {
                let notes = this.state.notes.filter( (note) => {
                    return note.isDone === false;
                });
                this.setState({
                    notes: notes,
                    filterNotes: notes,
                    activeNotes: notes.length
                });
            }
        }
    }

    // componentWillMount() {
    //     this.setState({
    //         notes: JSON.parse(localStorage.getItem("notes"))
    //     })
    // }

    render () {

        return (
            <div className="todoApp">
                <TodoInput onNoteAdd={this.handleAddNote}/>
                <TodoList 
                    notes={this.state.filterNotes}
                    deleteNote={this.handleDeleteNote}
                    completeNote={this.completeNote}
                />
                <TodoManager    
                    activeNotes={this.state.activeNotes}             
                    viewAllNotes={this.viewAllNotes}
                    viewCompletedNotes={this.viewCompletedNotes}
                    viewActiveNotes={this.viewActiveNotes}
                    deleteCompleted={this.deleteCompleted}
                />
            </div>
        )
    }
}

export default TodoApp;