import React from 'react'
import "./TodoInput.css";


class TodoInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ""
        };

        this.handleChangeArea = (event) => {
            this.setState({text: event.target.value});
        };

        this.AddNote = () => {
            if (this.state.text.length > 0) {
                let newNote = {
                    id: Date.now(),
                    text: this.state.text,
                    isDone: false
                }
                this.setState({
                    text: ""
                })
                this.props.onNoteAdd(newNote);
            } 
            
        }
        
    }


    render() {
        return (
            <div className="note_editor">
                <textarea 
                onChange={this.handleChangeArea} 
                value={this.state.text}
                placeholder="Input your task"
                />
                <button onClick={this.AddNote}>
                    Add
                </button>
            </div>
        )
    }
}

export default TodoInput;