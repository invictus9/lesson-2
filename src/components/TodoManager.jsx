import React from "react"
import "./TodoManager.css"

class TodoManager extends React.Component {
    constructor(props) {
        super(props);

    // this.getActiveNotesLength = () => {
    //     let notes = this.state.notes ? this.state.notes
    // }


    }

    render() {
        
        return (
            <div className="noteManager">
                <span>{this.props.activeNotes} items left</span>
                <a onClick={this.props.viewAllNotes} href="#" className="buttonsTodoManager">all</a>
                <a onClick={this.props.viewActiveNotes} href="#" className="buttonsTodoManager">Active</a>
                <a onClick={this.props.viewCompletedNotes} href="#" className="buttonsTodoManager">Completed</a>
                <a onClick={this.props.deleteCompleted} href="#" className="buttonDeleteCompleted">Delete completed </a>
            </div>
        )
    }
}

export default TodoManager;