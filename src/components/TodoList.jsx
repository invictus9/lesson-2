import React from "react";
import "./TodoList.css";
import TodoNote from "./TodoNote.jsx"; 

class TodoList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let notes = this.props.notes;
        if (notes !== null || notes.length !==0 ) {
            var allNotes = notes.map((note) => {
                return (
                    <TodoNote
                        key={note.id}
                        id={note.id}
                        note={note}
                        deleteNote={this.props.deleteNote}
                        completeNote={this.props.completeNote}
                    />
                )
            })
        } else {
            allNotes = 'Нет задании'
        }
        return(
                <div  className="todoList">
                    {
                        allNotes
                    }
                </div>
            
        )
    }
    
}

export default TodoList;