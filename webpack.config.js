
'use strict';

module.exports = {
    entry: "./src/app.jsx",
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    module: {
        loaders: [
            {
                test:/\.js$/,
                loader: "babel-loader",
                exclude:[/node_modules/, /dist/],
            },
            {
                test:/\.jsx$/,
                loader: "babel-loader",
                exclude:[/node_modules/, /dist/],
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader",
                exclude: [/node_modules/, /dist/]
            },
        ]
    }
};